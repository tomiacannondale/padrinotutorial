FactoryGirl.define do
  factory :first_post, :class => Post do |p|
    p.title 'first post'
    p.body  'nice post!'
    p.account { Account.find_by_email(Factory.build(:account).email) || Factory(:account) }
  end

  factory :second_post, :class => Post do |p|
    p.title 'second post'
    p.body 'good post!'
    p.account { Account.find_by_email(Factory.build(:account).email) || Factory(:account) }
  end
end
