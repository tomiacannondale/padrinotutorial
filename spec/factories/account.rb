FactoryGirl.define do
  factory :account do |a|
    a.name "Foo"
    a.surname "Bar"
    a.email "padrino@example.com"
    a.crypted_password ::BCrypt::Password.create("padrino")
    a.role "admin"
  end
end
