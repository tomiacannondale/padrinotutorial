require 'spec_helper'

describe "Post Model" do
  let(:post) { Post.new }
  it 'can be created' do
    post.should_not be_nil
  end

  describe "validations" do
    context "not title" do
      before do
        @post = Post.new(:body => "hoge")
      end

      it { @post.should_not be_valid }
    end

    context "not body" do
      before do
        @post = Post.new(:title => "hoge")
      end

      it { @post.should_not be_valid }
    end
  end
end
