require File.expand_path('../spec_helper', File.dirname(__FILE__))

describe PadrinoTutorial, :type => :acceptance do
  include Capybara::DSL
  before do
    Factory(:first_post)
    Factory(:second_post)
    visit "/posts"
  end

  describe "index" do
    it "find all posts" do
      all('.post').should have(2).items
    end

    include_examples "contain created user"

  end

  describe "show" do
    before do
      click_link "first post"
    end

    it "show first post" do
      within("#show .post .title") do
        page.should have_content "first post"
      end
    end

    include_examples "contain created user"

  end
end
