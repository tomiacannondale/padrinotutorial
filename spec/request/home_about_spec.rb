require File.expand_path('../spec_helper', File.dirname(__FILE__))
describe PadrinoTutorial, :type => :acceptance do
  include Capybara::DSL

  describe "/" do
    before do
      visit "/"
    end

    it { page.should have_content "Hello World!" }
  end

  describe "/about_us" do
    before do
      visit "/about_us"
    end

    it { page.should have_content "This is a sample blog created to demonstrate the power of Padrino!" }
  end

end
