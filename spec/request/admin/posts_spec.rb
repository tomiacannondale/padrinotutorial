require File.expand_path('../../spec_helper', File.dirname(__FILE__))

describe PadrinoTutorial, :type => :acceptance do
  describe "Admin::Post" do
    before do
      Factory(:account)
      visit '/admin'
      fill_in "email", with: "padrino@example.com"
      fill_in "password", with: "padrino"

      click_button "Sign In"
      within("#main-navigation") do
        click_link "Posts"
      end
    end

    describe "index" do
      it { page.should have_content "All Posts" }
    end

    describe "create post" do
      before do
        visit "/admin/posts/new"
        fill_in "Title", with: "admin post"
        fill_in "Body", with: "from admin/post"
        click_button "Save"
      end

      it { page.should have_content "Post was successfully created" }
      it { page.should have_content "Edit Post"}

      describe "association of account" do
        before do
          visit "/posts"
        end

        include_examples "contain created user"

      end

    end
  end
end
