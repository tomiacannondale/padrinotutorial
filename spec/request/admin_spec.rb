# -*- coding: utf-8 -*-
require File.expand_path('../spec_helper', File.dirname(__FILE__))

describe PadrinoTutorial, :type => :acceptance do
  describe Admin do
    include Capybara::DSL

    describe "login" do
      before do
        Factory(:account)
      end

      context "valid login" do
        before do
          visit '/admin'
        end

        it { page.should have_content 'Login Box' }

        before do
          visit '/admin/session/new'
          fill_in "email", with: "padrino@example.com"
          fill_in "password", with: "padrino"
        end

        it {
          click_button "Sign In"
          page.should have_content "Dashboard"
        }
      end
    end
  end
end
