PADRINO_ENV = 'test' unless defined?(PADRINO_ENV)
require File.expand_path(File.dirname(__FILE__) + "/../config/boot")
require 'capybara'
require 'capybara/rspec'
require 'capybara/dsl'
require 'factory_girl'
require 'database_cleaner'

Capybara.app = Padrino.application

RSpec.configure do |conf|
  conf.include Rack::Test::Methods

  conf.before(:suite) do
    DatabaseCleaner.strategy = :truncation
  end

  conf.before(:each) do
    DatabaseCleaner.start
  end

  conf.after(:each) do
    DatabaseCleaner.clean
  end
end

FactoryGirl.definition_file_paths = [
  File.join(Padrino.root, 'spec', 'factories')
]
FactoryGirl.find_definitions

def app
  ##
  # You can handle all padrino applications using instead:
  #   Padrino.application
  PadrinoTutorial.tap { |app|  }
end

shared_examples "contain created user" do
  it "should contain created user" do
    within(".details .author") do
      page.should have_content "padrino@example.com"
    end
  end
end
